package coding_challenge;

import coding_challenge.data.Animal;
import coding_challenge.util.Lions;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;

class LionTest {
    private String animalsNoLionsString = "[{\"id\": 123,\"category\": {\"id\": 1,\"name\": \"Dogs\"},\"name\": \"Bello\",\"photoUrls\": [\"dog.html\"],\"tags\": [{\"id\": 0,\"name\": \"string\"}],\"status\": \"available\"},{\"id\": 456,\"category\": {\"id\": 1,\"name\": \"Bird\"},\"name\": \"Birdy\",\"photoUrls\": [\"bird.html\"],\"tags\": [{\"id\": 0,\"name\": \"Bird\"}],\"status\": \"available\"},{\"id\": 789,\"category\": {\"id\": 1,\"name\": \"Cats\"},\"name\": \"Mautz\",\"photoUrls\": [\"cat.html\"],\"tags\": [{\"id\": 0,\"name\": \"string\"}],\"status\": \"available\"}]";
    private String animalsLionsString = "[{\"id\": 123,\"category\": {\"id\": 1,\"name\": \"Dogs\"},\"name\": \"Bello\",\"photoUrls\": [\"dog.html\"],\"tags\": [{\"id\": 0,\"name\": \"string\"}],\"status\": \"available\"},{\"id\": 456,\"category\": {\"id\": 1,\"name\": \"Bird\"},\"name\": \"Birdy\",\"photoUrls\": [\"bird.html\"],\"tags\": [{\"id\": 0,\"name\": \"Bird\"}],\"status\": \"available\"},{\"id\": 789,\"category\": {\"id\": 1,\"name\": \"Lion\"},\"name\": \"King\",\"photoUrls\": [\"lion.html\"],\"tags\": [{\"id\": 0,\"name\": \"string\"}],\"status\": \"available\"}]";

    private List<Animal> animalsNoLions;
    private List<Animal> animalsLions;

    @BeforeEach
    void prep() {
        // that passively already tests the transform method
        animalsNoLions = Lions.transformStringIntoListOfAnimals(animalsNoLionsString);
        animalsLions = Lions.transformStringIntoListOfAnimals(animalsLionsString);
    }

    @Test
    @DisplayName("should find no lion")
    public void shouldFindNoLion() {
        Assertions.assertTrue(Lions.searchForLions(animalsNoLions).isEmpty());
    }

    @Test
    @DisplayName("should find lion")
    public void shouldFindLion() {
        Assertions.assertFalse(Lions.searchForLions(animalsLions).isEmpty());
    }
}
