package coding_challenge;

import coding_challenge.util.Connection;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class ConnectionTest {
    @Test
    @DisplayName("should test if a given INT is 200")
    void shouldShowIfANumberHasValue200() {
        Assertions.assertTrue(Connection.isStatusCode200(200));
    }

    @Test
    @DisplayName("should show given INT is not 200")
    void shouldShowIfANumberHasNotValue200() {
        Assertions.assertFalse(Connection.isStatusCode200(100));
    }
}
