package coding_challenge;

import coding_challenge.data.Animal;
import coding_challenge.util.Connection;
import coding_challenge.util.Lions;

import java.net.http.HttpResponse;
import java.util.List;

public class App {
    public static void main(String[] args) throws Exception {
        // get request for animals
        HttpResponse<String> response = Connection.getAnimals();
        // check if the request was successful
        Connection.isStatusCode200(response.statusCode());
        // check if the response time is under 200
        Connection.isResponseTimeFasterThan200MS();

        // transform the response into a list of animals
        List<Animal> animals = Lions.transformStringIntoListOfAnimals(response.body());
        // check if any lions are among them
        List<Animal> lions = Lions.searchForLions(animals);

        // print the found lions, if there are any
        if (lions == null || lions.isEmpty()) {
            System.out.println("No lions today.");
        } else {
            for (Animal lion : lions) {
                System.out.println("Name: " + lion.getName() + ", ID: " + lion.getId());
            }
        }
    }
}
