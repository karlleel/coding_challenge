package coding_challenge.data;

import java.util.List;

/*
 * A data class to represent the animals. It mirrors the given
 * data structure from the API.
 */
public class Animal {
    long id;
    Category category;
    String name;
    List<String> photoUrls;
    List<Tag> tags;
    String status;

    public Animal(long id, Category category, String name, List<String> photoUrls, List<Tag> tags, String status) {
        this.id = id;
        this.category = category;
        this.name = name;
        this.photoUrls = photoUrls;
        this.tags = tags;
        this.status = status;
    }

    public long getId() {
        return id;
    }

    public Category getCategory() {
        return category;
    }

    public String getName() {
        return name;
    }

    public List<String> getPhotoUrls() {
        return photoUrls;
    }

    public List<Tag> getTags() {
        return tags;
    }
}
