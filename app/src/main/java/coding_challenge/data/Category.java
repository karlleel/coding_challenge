package coding_challenge.data;

/*
 * A data class to represent a subsection - Category - of
 * the Animal data class.
 */
public class Category {
    long id;
    String name;

    public Category(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
