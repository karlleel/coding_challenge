package coding_challenge.data;

/*
 * A data class to represent a subsection - Tag - of
 * the Animal data class.
 */
public class Tag {
    long id;
    String name;

    public Tag(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
