package coding_challenge.util;

import coding_challenge.data.Animal;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/*
 * A util class for checking on lions.
 */
public class Lions {
    private static final String LION = "lion";

    static Predicate<Animal> isNamedLion = a
            -> a.getName() != null
            && a.getName().toLowerCase().contains(LION);

    static Predicate<Animal> isCategoryLion = a
            -> a.getCategory() != null
            && a.getCategory().getName() != null
            && a.getCategory().getName().toLowerCase().contains(LION);

    static Predicate<Animal> isPhotoUrlLion = a
            -> a.getPhotoUrls() != null
            && !a.getPhotoUrls().isEmpty()
            && a.getPhotoUrls().stream().anyMatch(b
            -> b != null
            && b.toLowerCase().contains(LION));

    static Predicate<Animal> isTagLion = a
            -> a.getTags() != null
            && !a.getTags().isEmpty()
            && a.getTags().stream().anyMatch(b
            -> b.getName() != null
            && b.getName().toLowerCase().contains(LION));

    public static List<Animal> searchForLions(List<Animal> animals) {
        // using a Java stream to filter for lions.
        // multiple conditions are used, since the
        // required info can be found in different
        // places
        List<Animal> lions = animals.stream()
                .filter(Objects::nonNull)
                .filter(isNamedLion
                        .or(isCategoryLion)
                        .or(isPhotoUrlLion)
                        .or(isTagLion))
                .collect(Collectors.toList());

        return lions;
    }

    public static List<Animal> transformStringIntoListOfAnimals(String string) {
        // Gson, from google to handle json in Java
        Gson gson = new Gson();
        Type listOfAnimals = new TypeToken<ArrayList<Animal>>() {
        }.getType();

        // mapping the json to a list animal class
        return gson.fromJson(string, listOfAnimals);
    }
}
