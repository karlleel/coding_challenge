package coding_challenge.util;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

/*
 * A util class for connections. Not very generic, but does the
 * job for this small project.
 */
public class Connection {
    private static final String baseURL = "https://petstore3.swagger.io/api/v3";
    private static final String endpoint = "/pet/findByStatus";
    private static final String parameter = "?status=available";
    private static final String URL = baseURL + endpoint + parameter;

    private static long responseTime;

    public static HttpResponse<String> getAnimals() throws Exception {
        // prepare a new http call
        HttpClient httpClient = HttpClient.newBuilder().version(HttpClient.Version.HTTP_2).build();
        // setting couple of needed info, header, type, url
        HttpRequest request = HttpRequest.newBuilder().GET().uri(URI.create(URL)).header("accept", "application/json").build();

        // take the time in milliseconds directly before the call
        long milliStart = System.currentTimeMillis();
        // the actual GET call
        HttpResponse response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        // take the time in milliseconds directly after the call
        long milliEnd = System.currentTimeMillis();

        // do a little math ...
        responseTime = milliEnd - milliStart;

        return response;
    }

    public static boolean isStatusCode200(int code) {
        boolean res = code == 200;
        System.out.println("Status code: " + code);
        System.out.println("Is that what we want: " + (res ? "Hell yeah!" : "Nah, but it's something I suppose..."));
        System.out.println();
        return res;
    }

    // this method is not really precise, but the best I could find
    // for the default use of java
    public static void isResponseTimeFasterThan200MS() {
        System.out.println("Response Time for the last call: " + responseTime);
        System.out.println("Is that what we want: " + (responseTime <= 200 ? "Hell yeah!" : "To slow"));
        System.out.println();
    }
}
