# coding_challenge
In this repository you will find one possible solution of the following task:

# Task
* Please write a script to retrieve JSON data from a REST API and validate the response.
* Please use the following API Swagger UI (Pet store)
* Requirements
    1. Do a GET request to the /pet/findByStatus endpoint
    2. Validate the response code to be 200
    3. Validate the response time to be less than 200 ms
    4. Iterate over all elements of the json response body and print out all pets that are Lions.
* Consideration
    * Make it easy to build and run, do also consider maintainability
    * Think of further scenarios 
    * Don't be afraid to surprise us!
    * Documentation is always welcomed
    
# Info and decisions
1. To solve this task, the following tech stack is used:
    * Java - base language
    * JUnit 5 - test framework
    * Gradle - building tool
2. While there are three possible values for the parameter of the endpoint, it was assumed, that only the `available` lions are of interest.
3. The result list consists of the names of the lions that could be found and also the ID
4. There are four major parts to this solution attempt. The app, the util functions, the data structures and the tests.
   * The app does all the heavy lifting, so the task can be 'seen' in one simple execution.
   * The tests only check the util functions and passively the data structures. It was restrained from testing the actual API call, since that should be done by the API provider. So rather the handling of the response is being covered by tests.
5. I aimed for a naming convention in clean code style, but since it was of interest, commands are also available in interesting places. :)

# How to use
Please make sure, that gradle is set up on your machine.

Then please start the task

`Gradle -> Task -> application -> run`

Alternatively the command line can be used from within the project folder

`gradle clean run`

The tests can be run from the test files themselves. Tho they should be runnable by gradle (build.gradle - task `test` definition), I could not get it to work in time. So either right click the file and click `run` for all tests in the class, or the `green triangle` on a single method. (This might visually differ depending on the used IDE.) 